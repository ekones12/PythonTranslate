#------------------------------------------------------------#
# Student Name: Muhammed Enes Koçak
# Student ID:   b21427119
# BBM103 Introduction to Programming Laboratory I, Fall 2016 
# Assignment 3: Mission: Save the Earth
#------------------------------------------------------------#

# Importing the sys module that supports reading command-line arguments
import sys
x=sys.argv[1]
y=sys.argv[2]
z=sys.argv[3]
Dict={}
Dict2={}
def read_dictionary():
    c= open(x, "r")
    for line in c :
        items = line.split()
        keys,values=items[0],items[1]
        Dict.setdefault(keys,values)
        Dict2.setdefault(values, keys)
    c.close()

def binarian_to_english():
    read_dictionary()
    asd = []
    text=open(y,"r")
    for i in text:
        items  = i.split()
        items2 = i.split()
        if items[0][0]!="+" and items[0][0]!="#" :
            for a in range(0,len(items)):
                if items[a] in Dict:
                    asd+=[Dict[items[a]]]
                    if a == len(items)-1:
                        asd[len(asd)-1]+="\n"
                    else:
                        asd[len(asd)-1]+=" "
                else:
                    asd += items2[a]
                    if a == len(items) - 1:
                        asd[len(asd) - 1] += "\n"
                    else:
                        asd[len(asd) - 1] += " "
        if items[0][0]=="+":
            for a in range(0,len(items)):
                if "poaD" in items:
                    if items[a][0] == '1':
                        temp = float(binary_to_decimal(items[a]))
                if "wovben" in items:
                    if items[a][0] == '1':
                        dist = float(ly_to_km(binary_to_decimal((items[a]))))
                if "bav'Do" in items:
                    if items[a][0] == '1':
                        speed = float(binary_to_decimal(items[a]))

    text3="Data about Binarian planet:\nDistance from the Earth: {} km\n" \
          "Planet temperature: {} degrees Celsius\n" \
          "Orbital speed: {} km/s\n".format(dist,temp,speed)
    text2 = "".join(asd)
    text.close()
    print(text2)
    print(text3)
    output1=open("binarian_message.txt","w")
    output2=open("computations.txt.","w")
    output2.write(text3)
    output1.write(text2)
    output1.close()
    output2.close()


def english_to_binarian():
    read_dictionary()
    text=open(z,"r")
    asd = []
    for i in text:
        items = i.split()
        items2 = i.split()
        if items[0][0] != "+" and items[0][0] != "#":
            for a in range(0, len(items)):
                numbers=['0','1','2','3','4','5','6','7','8','9']
                items[a] = items[a].strip(",")
                items[a] = items[a].strip(".")
                items[a] = items[a].strip("?")
                items[a] = items[a].strip("!")
                items[a] = items[a].lower()
                if items[a] in Dict2:
                    asd += [Dict2[items[a]]]
                    if a == len(items) - 1:
                        asd[len(asd) - 1] += "\n"
                    else:
                        asd[len(asd) - 1] += " "
                elif items[a][0] in numbers:
                    asd += [decimal_to_binary(items[a])+" "]

                else :
                    items2[a] = items2[a].strip(",")
                    items2[a] = items2[a].strip(".")
                    items2[a] = items2[a].strip("?")
                    items2[a] = items2[a].strip("!")
                    asd += items2[a]
                    if a == len(items) - 1:
                        asd[len(asd) - 1] += "\n"
                    else:
                        asd[len(asd) - 1] += " "
    text.close()
    text2 = "".join(asd)
    print(text2)
    output=open("message.txt.","w")
    output.write(text2)
    output.close()


def binary_to_decimal(number):
    total=0
    a=str(number)
    b=len(a)
    for c in a:
        b=b-1
        sum=int(c)*2**b
        total=total+sum
    return total

def decimal_to_binary(number):#This function takes a decimal number as input and returns its binary value.
    x = int(number)
    k = []
    while (x > 0):
        a = int(float(x % 2))
        k.append(a)
        x = (x - a) / 2
    binnumber = ""
    for i in k[::-1]:
        binnumber = binnumber + str(i)
    return binnumber

def ly_to_km(distance):#This function takes a distance in light-years and returns its value in kilometers.
    x=distance
    km=int(x)*300.000*365*24*60*60
    return km



binarian_to_english()
english_to_binarian()

